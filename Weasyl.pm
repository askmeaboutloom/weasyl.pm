package Weasyl;
$VERSION = '0.00_04';
$VERSION = eval $VERSION;
use strict;
use warnings;
use Log::Message::Simple;
use Mojo::DOM;
use JSON;
use URI;
use WWW::Mechanize;
use Exception::Class (
    Except        => {
        description => 'Base exception class for the FurAffinity module',
    },
    ArgExcept     => {
        isa         => 'Except',
        description => 'Incorrect or missing argument',
    },
    RequestExcept => {
        isa         => 'Except',
        description => 'Request failed',
        fields      => ['method'],
    },
    PageExcept    => {
        isa         => 'Except',
        description => "A page didn't look as expected",
    },
    FormExcept    => {
        isa         => 'Except',
        description => 'A form field was invalidated on the client side',
        fields      => ['field', 'check'],
    },
);

our $ws      = 'https://www.weasyl.com';
our $charset = 'utf-8';

our ($msg, $debug);


sub new {
    my ($class, $key) = @_;
    ArgExcept->throw(error => 'Missing API key.') if !$key || $key !~ /\S/;
    my $mech = WWW::Mechanize->new;
    $mech->add_header('X-Weasyl-API-Key', $key);
    bless {
        key       => $key,
        mech      => $mech,
    } => $class;
}

sub mech { $_[0]->{mech} }
sub key  { $_[0]->{key } }
sub dom  { Mojo::DOM->new($_[0]->mech->content(charset => $charset)) }

sub get {
    my $self = shift;
    my $mech = $self->mech;
    debug "get @_", $debug;
    $mech->get(@_);
    return $self->dom if $mech->success;
    RequestExcept->throw(method => 'GET', error => $mech->res->status_line);
};

sub post {
    my $self = shift;
    my $mech = $self->mech;
    debug "post @_", $debug;
    $mech->post(@_);
    return $self->dom if $mech->success;
    RequestExcept->throw(method => 'POST', error => $mech->res->status_line);
};

sub get_api {
    my $self = shift;
    my $mech = $self->mech;
    debug "get_api @_", $debug;

    my $url = URI->new(shift);
    $url->query_form(@_);
    $mech->get($url);

    return JSON->new->utf8->decode($mech->content(charset => $charset))
        if $mech->success;
    RequestExcept->throw(method => 'GET', error => $mech->res->status_line);
}


our %field_checkers = (
    max      => sub { length $_[0] <= $_[1]         },
    min      => sub { length $_[0] >= $_[1]         },
    regex    => sub { $_[0] =~ $_[1]                },
    inhash   => sub { exists $_[1]->{$_[0]}         },
    inlist   => sub { grep { $_ eq $_[0] } @{$_[1]} },
    exists   => sub { $_[1] ? -e $_[0] : !-e $_[0]  },
    optional => sub { 1                             },
);

sub check_fields {
    my ($fields, $restrictions) = @_;
    while (my ($name, $restrict) = each %$restrictions) {
        my $field = $fields->{$name};
        next if $restrict->{optional} && !defined $field;
        while (my ($k, $v) = each %$restrict) {
            if (!$field_checkers{$k}->($field, $v)) {
                FormExcept->throw(
                    error => "$name does not pass $k => $v with $field",
                    field => [$name => $field],
                    check => [$k    => $v    ],
                );
            }
        }
    }
}


sub fetch_user     { $_[0]->get_api("$ws/api/whoami") }

sub fetch_messages {
    my ($self)   = @_;
    my $messages = $self->get_api("$ws/api/messages/summary");

    $messages->{notes} = delete $messages->{unread_notes};

    my $total  = 0;
    for my $n (values %$messages) { $total += $n }
    $messages->{total} = $total;

    return $messages;
}

sub fetch_folders {
    my ($self, $dom) = @_;
    $dom //= $self->get("$ws/submit/visual");

    my %folders;
    for my $f ($dom->find('#submissionfolder option')->each) {
        $folders{$f->text} = $f->val . '';
    }
    return \%folders;
}

sub fetch_submissions {
    my $self    = shift;
    my %fields  = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;
    my $folders = $fields{folders} // $self->fetch_folders;

    check_fields(\%fields, {
        since    => {regex  => qr/\d+/,            optional => 1},
        count    => {min    => 1, max => 100,      optional => 1},
        folderid => {inlist => [values %$folders], optional => 1},
        folder   => {inhash => $folders,           optional => 1},
        backid   => {regex  => qr/\d+/,            optional => 1},
        nextid   => {regex  => qr/\d+/,            optional => 1},
    });

    my %params;
    for (qw(since count folderid backid nextid)) {
        $params{$_} = $fields{$_} if exists $fields{$_};
    }
    if (exists $fields{folder}) {
        $params{folderid} = $folders->{$fields{folder}};
    }

    return $self->get_api("$ws/api/users/$fields{user}/gallery", %params);
}

sub fetch_submission {
    my ($self, $id) = @_;
    return $self->get_api("$ws/api/submissions/$id/view");
}

sub fetch_info {
    my ($self, $id) = @_;
    my $dom = $self->get("$ws/edit/submission?submitid=$id");
    # TODO more fields
    return {
        title       => '' . $dom->find('#editsubmissiontitle')->val,
        description => '' . $dom->find('#editsubmissiondesc' )->text,
    };
}


sub change_info {
    my $self = shift;
    my $mech = $self->mech;
    my %fields = ref $_[0] eq 'HASH' ? %{$_[0]} : @_;

    check_fields(\%fields, {
        id          => {regex => qw/^\d+$/              },
        title       => {regex  => qr/\S/,  optional => 1},
        description => {regex  => qr/\S/,  optional => 1},
        # TODO other fields
    });

    my %form;
    $form{title   } = $fields{title      } if exists $fields{title      };
    $form{content } = $fields{description} if exists $fields{description};

    $self->get("$ws/edit/submission?submitid=$fields{id}")
        unless $fields{no_load};
    eval {
        $mech->submit_form(
            form_name => 'editsubmission',
            fields    => \%form,
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/submission/$fields{id}/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
}


our %ratings = (
    general  => 10,
    moderate => 20,
    mature   => 30,
    explicit => 40,
);

our %categories = (
    ''            => '',
    'traditional' => 1020,
    'digital'     => 1030,
);

sub submit_artwork {
    my $self  = shift;
    my $mech  = $self->mech;
    my %fields = (
        keywords => '',
        category => '',
        folder   => '',
        ref $_[0] eq 'HASH' ? %{$_[0]} : @_,
    );
    msg 'Submitting artwork...', $msg;

    my $dom     = $self->get("$ws/submit/visual");
    my $folders = $self->fetch_folders($dom);

    check_fields(\%fields, {
        file        => {exists => 1               },
        thumb       => {exists => 1, optional => 1},
        title       => {regex  => qr/\S/          },
        description => {regex  => qr/\S/          },
        keywords    => {regex  => qr/\S+\s+\S+/   },
        rating      => {inhash => \%ratings       },
        category    => {inhash => \%categories    },
        folder      => {inhash => $folders        },
    });

    eval {
        $mech->submit_form(
            form_id => 'submit-form',
            fields  => {
                title      => $fields{title},
                content    => $fields{description},
                tags       => $fields{keywords},
                subtype    => $categories{$fields{category}},
                rating     => $ratings{$fields{rating}},
                folderid   => $folders->{$fields{folder}},
                submitfile => $fields{file},
                defined $fields{thumb} ? (thumbnail => $fields{thumb}) : (),
            },
        );
    };
    PageExcept->throw(error => $@) if $@;

    $mech->uri =~ m{/submission/(\d+)/} or PageExcept->throw(
        error => 'Unexpected submission result URL: ' . $mech->uri,
    );
    return int $1;
}


1;
__END__

