Weasyl.pm
==============

A Perl module for interfacing with weasyl.com.

Still heavily under development!

You can get the module as a single file in the `dist` folder. Documentation is in the `doc` folder.
