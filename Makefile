all: dist doc

dist: dist/Weasyl.pm

doc: doc/Weasyl.html

clean:
	rm -rf dist


dist/Weasyl.pm: Weasyl.pm doc/Weasyl.pod
	mkdir -p dist
	cat Weasyl.pm doc/Weasyl.pod > $@

doc/Weasyl.html: doc/Weasyl.pod
	perl -MPod::Simple::HTML -e \
    '$$ps=Pod::Simple::HTML->new; $$ps->index(1); $$ps->parse_from_file;' \
    < doc/Weasyl.pod > doc/Weasyl.html

.PHONY: all clean dist doc
